-- SUMMARY --

Make D7 admin paths functionality available to D6.

For a full description of the module, visit the project page:
  http://drupal.org/project/admin_menu

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/admin_menu


-- REQUIREMENTS --

CTools module (ctools static implementation).


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- USAGE --

Backported functions:

* path_is_admin()
    Determine whether a path is in the administrative section of the site.

* path_get_admin_paths()
    Get a list of administrative and non-administrative paths.

* hook_admin_paths()
    Define administrative paths (http://api.drupal.org/api/drupal/modules%21system%21system.api.php/function/hook_admin_paths/7).
    Backported all hook_admin_paths() core modules. 


-- CONTACT --

Current maintainer:
* José Sánchez (ruloweb) - http://drupal.org/user/203115
